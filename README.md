# PurpleChat

PurpleChat, le Messenger de l'ESN.81, est actuellement en cours de développement. Il aura pour objectif de permettre de communiquer via un système de messagerie instantané et possibilité d'envoyer diverses pièces jointes, à savoir des vocaux, photos, vidéos, etc... Pour l'instant l'objectif premier est de le faire fonctionner à petite échelle, c'est-à-dire eau sein de notre équipe de développement, puis de l'étendre à notre promotion, et enfin pourquoi pas à toute l'école.

## L'équipe

* Marianne PELISSIER
* Remi BACQUEZ
* Léo LAMARQUE
* Arnaud CABANAC

## L'ESN.81 - PurpleCampus
 ![Logo_ESN81](https://media-exp1.licdn.com/dms/image/C4E0BAQEehInG8alXHw/company-logo_200_200/0/1583768651345?e=2159024400&v=beta&t=R56mSTauo9geWmGOaR9hBd1DHOGDfY6hbmQ34TtL9iQ) 